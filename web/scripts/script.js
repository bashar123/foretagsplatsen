var app = angular.module("app", ["ngRoute","chart.js"]);

app.config(function($routeProvider){
    $routeProvider
        .when("/history", {
        templateUrl: "templates/history.html"
        })
        .when("/main",{
            templateUrl: "templates/main.html"
        })
        .otherwise({
            redirectTo:"/main"
        });
});


app.controller("HistoryCtrl", function($scope, historyService){
    console.log("historyCtrl");
    $scope.history = historyService.history();
});


app.controller("PieCtrl", function ($scope, arrayService) {
    $scope.data = arrayService.data();
});


app.controller("UpploadCtrl", function ($scope,$http, arrayService, historyService) {
    $scope.fileName = "";

    $scope.uploadFile = function(files, text) {
        console.log("uploadfile");

        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);
        fd.append("text", $scope.fileName);

        $http.post("http://localhost:8084/rest/upload", fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function(res){
            console.log("successful");

            var keys = [];
            var values = [];
            for(var i = 0; i<res.length; i++){
               keys.push( res[i].key);
               values.push( res[i].value);
            }
            //set the new data to be shown
            arrayService.setData(keys, values, $scope.fileName);
            //record the upload
            historyService.newUpload(arrayService.data());

            //clear the filename input
            $scope.fileName = "";

        } ).error( function(){
            console.log("error");
        } );
    };
});


app.controller("nameCtrl", function ($scope, arrayService) {
    $scope.data = arrayService.data();

});


app.factory('arrayService', function(){

    var data = {
        keys   : [],
        values : [],
        name: ""
    };

    return{
        setData: function(keys, values, name){
            data.keys = keys;
            data.values = values;
            data.name = name;
        },
        data : function(){
            return data;
        }
    }
});


app.factory('historyService', function(){
    var history = [];

    return{
        newUpload: function(upload){
            console.log("newUpload");

            history.push({
                keys: upload.keys,
                values: upload.values,
                name: upload.name
            });
        },
        history: function(){
            return history;
        }
    }

});
