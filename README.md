## Foretagsplatsen webbapp ##

### About ###
A single page web application written with **AngularJS** (frontend), **Java** and **Jersey** for REST communication (backend) .

### Use ###
Browse a file to upload to the server and be parsed, the result is a pie-chart of the occurrences of hashtags.
A history of the uploaded files is recorded as well.