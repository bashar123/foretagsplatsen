package foretagsplatsen.buissness;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;


public class Model {

	/**
	 * Parse the conten of the filestream to find and store all the records in an array
	 * @param fis
	 * @return the result of the 'makeEntityList' method
	 */
	public static ArrayList<Entity> parseFile(InputStream fis){
		int content;
		boolean state = false;
		StringBuilder word = new StringBuilder();
		ArrayList<String> words = new ArrayList<>();

		try {
			while ((content = fis.read()) != -1) {
				if(content =='#'){
					state = true;
				}

				else if(state){
					if(content==' '){
						words.add(word.toString());
						word = new StringBuilder();
						state = false;
					}
					else{
						word.append((char) content);
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return makeEntityList(words);
	}


	/**
	 * Calculate the occurence of each element in words and stores the result in the returned array
	 * @param words
	 * @return ArrayList of the data statistics
	 */
	private static ArrayList<Entity> makeEntityList(ArrayList<String> words){
		ArrayList<Entity> entities = new ArrayList<>();

		Collections.sort(words);

		int num = 1;
		for (int i = 1; i<words.size(); i++){
			if(words.get(i).equals(words.get(i-1))){
				num++;
			}
			else {
				entities.add(new Entity(words.get(i-1), num));
				num = 1;
			}
			if(i==(words.size()-1)){
				entities.add(new Entity(words.get(i), num));
			}

		}
		return entities;
	}

}
