package foretagsplatsen.services;

import com.sun.jersey.multipart.FormDataParam;
import foretagsplatsen.buissness.Entity;
import foretagsplatsen.buissness.Model;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;


@Path("/upload")
public class FileService {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public ArrayList<Entity> getClichedMessage(
			@FormDataParam("file") InputStream fis,
			@FormDataParam("text") String fileName) {

		System.out.println("File name: " + fileName);

		ArrayList<Entity> entities = Model.parseFile(fis);

		return entities;
	}
}
